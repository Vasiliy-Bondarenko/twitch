#### How would you deploy the above on AWS?

- Any sql server depending on expected usage.
- Load balancer and one EC2 instance behind it. Automatic scaling if needed. This app does not use sessions, so it's basically "stateless" from app-server point of view.

#### Where do you see bottlenecks in your proposed architecture and how would you approach scaling this app starting from 100 reqs/day to 900MM reqs/day over 6 months?

There is not much bottlenecks here.
App server can be easily scaled horizontally by adding more EC2 instances.  
If DB becomes a problem (which is unlikely) it can be sharded by user's login.  
We can use any Websocket adapter supported by Laravel depending or app usage. Let's say Pusher can scale required.
