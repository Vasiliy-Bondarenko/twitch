<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Twitch Viewer</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <style>
        pre {
            background-color: gray;
            padding: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <iframe
                    src="https://player.twitch.tv/?channel={{$favorite}}&muted=true"
                    height="400" width="600" scrolling="no"
            ></iframe>
        </div>
        <div class="col">
            <iframe scrolling="no"
                    id="chat_embed"
                    src="https://www.twitch.tv/embed/{{$favorite}}/chat"
                    height="500" width="350"
            ></iframe>
            <div id="app">
                <event-log></event-log>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
    window.storage = {
        favorite: "{{ $favorite }}"
    }
</script>
<script src="<?= mix("/js/app.js") ?>"></script>
</body>
</html>
