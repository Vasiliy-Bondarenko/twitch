<?php

namespace App\Http\Controllers;

use App\Events\TwitchEvent;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TwitchController extends Controller
{
    public function homepage()
    {
        return view('welcome');
    }

    public function login_ok(Request $request, Client $client)
    {
        $code = $request->query("code");
        $url  = "https://id.twitch.tv/oauth2/token?"
                . "client_id=" . env("TWITCH_CLIENT_ID")
                . "&client_secret=" . env("TWITCH_SECRET")
                . "&code=" . $code
                . "&grant_type=authorization_code"
                . "&redirect_uri=" . urlencode(env("TWITCH_REDIRECT_URL"));

        $res = $client->request('POST', $url);
        if ($res->getStatusCode() !== 200) {
            throw new \RuntimeException("Token request failed.");
        }

        /*
           Response Example:
          {
              "access_token": "<user access token>",
              "refresh_token": "<refresh token>",
              "expires_in": <number of seconds until the token expires>,
              "scope": ["<your previously listed scope(s)>"],
              "token_type": "bearer"
            }
         */
        $data = json_decode($res->getBody()->getContents());

        $access_token = $data->access_token;
        // $data->refresh_token;

        $url = "https://api.twitch.tv/helix/users";

        $res = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $access_token,
            ]
        ]);
        if ($res->getStatusCode() !== 200) {
            throw new \RuntimeException("Get user's info failed.");
        }

        $data = json_decode($res->getBody()->getContents())->data[0];

        // register local user
        $user = User::updateOrCreate([
            "twitch_id" => $data->id
        ], [
            "login" => $data->login,
            "token" => $access_token,
        ]);

        return redirect("/set_favorite")->withCookie(cookie()->forever('access_token', $access_token));
    }

    public function dashboard()
    {
        $user = $this->current_user();

        return view("dashboard")->with(["favorite" => $user->favorite]);
    }

    protected function subscribe_to_topic(string $topic, $callback)
    {
        $client = app(Client::class);
        $url = "https://api.twitch.tv/helix/webhooks/hub";
        $res = $client->post($url, [
            "json"    => [
                "hub.callback"      => $callback,
                "hub.mode"          => "subscribe",
                "hub.lease_seconds" => 864000,
                "hub.secret"        => md5(env("APP_KEY")),
                "hub.topic"         => $topic,
            ],
            "headers" => [
                "Client-ID" => env("TWITCH_CLIENT_ID"),
            ]
        ]);

        if ($res->getStatusCode() !== 202) {
            throw new \RuntimeException("Can't subscribe: " . $res->getBody()->getContents());
        }
    }

    public function events(Request $request, $favorite)
    {
        if ($request->hub_mode === "subscribe") {
            Log::info("Confirming hook " . $request->hub_challenge);
            return $request->hub_challenge;
        }

        // todo: verify with X-Hub-Signature header: https://dev.twitch.tv/docs/api/webhooks-reference/#subscribe-tounsubscribe-from-events

        event(new TwitchEvent(json_encode($request->all()), $favorite));

        Log::info("Got hook " . json_encode($request->all()));
    }

    public function set_favorite_form()
    {
        return view("set_favorite");
    }

    public function set_favorite(Request $request, Client $client)
    {
        $user           = $this->current_user();

        $url = "https://api.twitch.tv/helix/users?login=" . $request->favorite;

        $res = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $user->token,
            ]
        ]);
        if ($res->getStatusCode() !== 200) {
            throw new \RuntimeException("Get user's info failed.");
        }

        $data = json_decode($res->getBody()->getContents())->data[0];

        $user->favorite = $request->favorite;
        $user->favorite_id = $data->id;
        $user->save();

        // todo: throw it into queue and run async
        $topics = [
            "https://api.twitch.tv/helix/users/follows?first=1&to_id=" . $user->favorite_id
            => env("TWITCH_HOOKS_EVENTS_URL") . "/" . $user->favorite
            // list of available events: https://dev.twitch.tv/docs/api/webhooks-reference/
        ];
        foreach ($topics as $topic => $callback) {
            $this->subscribe_to_topic($topic, $callback);
        }

        return redirect("/dashboard");
    }

    protected function current_user(): ?User
    {
        return User::whereToken(request()->cookie("access_token"))->first();
    }
}
