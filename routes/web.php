<?php

Route::get('/', "TwitchController@homepage");
Route::get('/login_ok', "TwitchController@login_ok");
Route::get('/set_favorite', "TwitchController@set_favorite_form");
Route::post('/set_favorite', "TwitchController@set_favorite");
Route::get('/dashboard', "TwitchController@dashboard");
Route::get('/hooks/events', "TwitchController@events");
Route::any('/hooks/events/{favorite}', "TwitchController@events");


